#include <stdlib.h>
#include <signal.h>
#include <algorithm>
#include <sstream>

#include "generic/config.h"
#include "generic/screen_switcher.h"

#include "global.h"
#include "search_dialog.h"
#include "title.h"

SearchDialog *searchDialog;

using namespace Generic;
using namespace Search;

SearchDialog::SearchDialog()
{
    w = NC::ListBase<SearchDialogItemBaseRef>(0, MainStartY, COLS, MainHeight, "", NC::Color::Yellow, NC::Border::None);
    w.cyclicScrolling(false);
    w.centeredCursor(false);
    w.setHighlightColor(NC::Color::Yellow);
    //w.setSelectedPrefix("");
    //w.setSelectedSuffix("");
    w.setItemDisplayer(std::bind(
        [](NC::ListBase<SearchDialogItemBaseRef> &list) 
        { 
            SearchDialogItemBaseRef item = list.drawn()->value();
            item->render(list);
        }
        , std::placeholders::_1));
    
    w.showAll();
}

void SearchDialog::switchTo()
{
	SwitchTo::execute(this);
	drawHeader();
}

void SearchDialog::resize()
{
	size_t x_offset, width;
	getWindowResizeParams(x_offset, width);
	w.resize(width, MainHeight);
	w.moveTo(x_offset, MainStartY);

	//if (Config.columns_in_playlist && Config.titles_visibility)
	//	w.setTitle(Display::Columns(w.getWidth()));
	//else
	//	w.setTitle("");
        w.setTitle("");
	
	hasToBeResized = 0;
}

std::wstring SearchDialog::title()
{
	std::wstring result = L"Search result ";
	return result;
}

void edit(const SearchDialogItemBaseRef& searchDialogItem)
{
    std::stringstream command;

    const Search::SearchResultItem& item = searchDialogItem->getSearchResultItem();
    if (searchDialogItem->getType() == SearchDialogItemType::Context)
    {
        command << "vim -p " 
                //<< "+" << item.line << " "
                << "-c " << "'normal " 
                    << item.line << "G" << item.start << "l" 
                    //<< "v"  
                    //<< item.line << "G" << item.start << "l"
                    //<< (item.end - item.start) << "l"
                    << "' "
                << item.filename;
    }
    else if (searchDialogItem->getType() == SearchDialogItemType::Filename)
    {
        command << "vim -p " 
                << item.filename;
    }
    else
    {
        return;
    }
    system(command.str().c_str());
    raise(SIGUSR1);
}

void SearchDialog::enterPressed()
{
	if (!w.empty())
		edit(w.current().value());
    
    
}

void SearchDialog::spacePressed()
{
    if (!w.empty())
    {
        w.current().setSelected(!w.current().isSelected());
        w.scroll(NC::Scroll::Down);
    }	
}

void SearchDialog::mouseButtonPressed(MEVENT me)
{
    if (!w.empty() && w.hasCoords(me.x, me.y))
    {
        if (size_t(me.y) < w.size() && (me.bstate & (BUTTON1_PRESSED | BUTTON3_PRESSED)))
        {
            w.Goto(me.y);
            if (me.bstate & BUTTON3_PRESSED)
                    enterPressed();
        }
        else
            Screen<WindowType>::mouseButtonPressed(me);
    }
}

/***********************************************************************/

