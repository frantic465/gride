#pragma once

#include <unordered_map>

#include "generic/screen.h"
#include "generic/list_base.h"

#include "search_dialog_item.h"
#include "search/grep.h"

struct SearchDialog: Generic::Screen< Generic::NC::ListBase<SearchDialogItemBaseRef> >
{
	SearchDialog();
	
	// Screen<NC::Menu<MPD::Song>> implementation
	virtual void switchTo() OVERRIDE;
	virtual void resize() OVERRIDE;
	
	virtual std::wstring title() OVERRIDE;
	virtual Generic::ScreenType type() OVERRIDE { return Generic::ScreenType::SearchResult; }
	
	virtual void update() OVERRIDE { }
	
	virtual void enterPressed() OVERRIDE;
	virtual void spacePressed() OVERRIDE;
	virtual void mouseButtonPressed(MEVENT me) OVERRIDE;
	
	virtual bool isMergable() OVERRIDE { return true; }
	
	
protected:
	virtual bool isLockable() OVERRIDE { return true; }
	
private:
	
};

extern SearchDialog *searchDialog;

