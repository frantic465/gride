#pragma once

#include "config.h"

namespace Generic {

class SwitchTo
{
	template <bool ToBeExecuted, typename ScreenT> struct TabbableAction_ {
		static void execute(ScreenT *) { }
	};
	
public:
	template <typename ScreenT>
	static void execute(ScreenT *screen)
	{
		if (screen->hasToBeResized)
			screen->resize();
		
		setActiveScreen(screen);
	}
};

}; // end of namespace Generic