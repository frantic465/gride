#include <cassert>

#include "screen.h"

namespace Generic {//

void drawScreenSeparator(int x)
{
	attron(COLOR_PAIR(int(COLOR_CYAN)));
	mvvline(MainStartY, x, 0, MainHeight);
	attroff(COLOR_PAIR(int(COLOR_BLUE)));
	refresh();
}

void genericMouseButtonPressed(NC::Window &w, MEVENT me)
{
	if (me.bstate & BUTTON2_PRESSED)
	{
		if (true)//Config.mouse_list_scroll_whole_page)
			w.scroll(NC::Scroll::PageDown);
		else
			for (size_t i = 0; i < 10/*Config.lines_scrolled*/; ++i)
				w.scroll(NC::Scroll::Down);
	}
	else if (me.bstate & BUTTON4_PRESSED)
	{
		if (true)//Config.mouse_list_scroll_whole_page)
			w.scroll(NC::Scroll::PageUp);
		else
			for (size_t i = 0; i < 10/*Config.lines_scrolled*/; ++i)
				w.scroll(NC::Scroll::Up);
	}
}

void scrollpadMouseButtonPressed(NC::Scrollpad &w, MEVENT me)
{
	if (me.bstate & BUTTON2_PRESSED)
	{
		for (size_t i = 0; i < 10/*Config.lines_scrolled*/; ++i)
			w.scroll(NC::Scroll::Down);
	}
	else if (me.bstate & BUTTON4_PRESSED)
	{
		for (size_t i = 0; i < 10/*Config.lines_scrolled*/; ++i)
			w.scroll(NC::Scroll::Up);
	}
}

/***********************************************************************/

void BaseScreen::getWindowResizeParams(size_t &x_offset, size_t &width, bool adjust_locked_screen)
{
	width = COLS;
	x_offset = 0;
}

/***********************************************************************/

Generic::BaseScreen *myScreen;
size_t MainStartY;
size_t MainHeight;

void applyToVisibleWindows(void (BaseScreen::*f)())
{
	(getVisibleScreen()->*f)();
}

bool isVisible(BaseScreen *screen)
{
	assert(screen != 0);
	return screen == getVisibleScreen();
}

void validateScreenSize()
{
    if (COLS < 30 || MainHeight < 5)
    {
        NC::destroyScreen();
        std::cout << "Screen is too small to handle" APPNAME "correctly\n";
        exit(1);
    }
}

void setWindowsDimensions(size_t left, size_t top, size_t right, size_t bottom)
{
    MainStartY = top;
    MainHeight = bottom;
}

BaseScreen* getVisibleScreen()
{
    return myScreen;
}

void setActiveScreen(BaseScreen* screen)
{
    myScreen = screen;
}

}; // end of namespace Generic
