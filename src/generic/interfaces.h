#pragma once

#include <string>
#include "gcc.h"

namespace Generic {

struct Searchable
{
	virtual bool allowsSearching() = 0;
	virtual bool search(const std::string &constraint) = 0;
	virtual void nextFound(bool wrap) = 0;
	virtual void prevFound(bool wrap) = 0;
};

}; // end of namespace Generic