#include <boost/locale/encoding.hpp>
#include "wide_string.h"

std::string ToString(std::wstring ws)
{
	return boost::locale::conv::utf_to_utf<char>(ws);
}

std::wstring ToWString(std::string s)
{
	return boost::locale::conv::utf_to_utf<wchar_t>(s);
}

size_t wideLength(const std::wstring &ws)
{
	int len = wcswidth(ws.c_str(), -1);
	if (len < 0)
		return ws.length();
	else
		return len;
}

void wideCut(std::wstring &ws, size_t max_length)
{
	size_t i = 0;
	int remained_len = max_length;
	for (; i < ws.length(); ++i)
	{
		remained_len -= wcwidth(ws[i]);
		if (remained_len < 0)
		{
			ws.resize(i);
			break;
		}
	}
}

std::wstring wideShorten(const std::wstring &ws, size_t max_length)
{
	std::wstring result;
	if (wideLength(ws) > max_length)
	{
		const size_t half_max = max_length/2 - 1;
		size_t len = 0;
		// get beginning of string
		for (auto it = ws.begin(); it != ws.end(); ++it)
		{
			len += wcwidth(*it);
			if (len > half_max)
				break;
			result += *it;
		}
		len = 0;
		std::wstring end;
		// get end of string in reverse order
		for (auto it = ws.rbegin(); it != ws.rend(); ++it)
		{
			len += wcwidth(*it);
			if (len > half_max)
				break;
			end += *it;
		}
		// apply end of string to its beginning
		result += L"..";
		result.append(end.rbegin(), end.rend());
	}
	else
		result = ws;
	return result;
}

