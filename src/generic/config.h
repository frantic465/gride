#pragma once
#include <stdlib.h>


#define APPNAME "gride"

namespace Generic {
    
class BaseScreen;
namespace NC
{
    class Window;
}   
    
enum class ActionType
{
	MacroUtility, Dummy, MouseEvent, ScrollUp, ScrollDown, PageUp, PageDown, PressEnter, PressSpace, 
    ExecuteCommand, FindItemForward, FindItemBackward,
	NextFoundItem, PreviousFoundItem, ToggleFindMode, Quit, 
};

enum class ScreenType
{
	SearchResult,
};

}; // end of namespace Generic