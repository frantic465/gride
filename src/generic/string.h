#pragma once

#include <cstdarg>
#include <locale>
#include <string>
#include <vector>
#include "gcc.h"

template <size_t N> size_t const_strlen(const char (&)[N]) {
	return N-1;
}

bool isInteger(const char *s, bool accept_signed);

std::string getBasename(const std::string &path);
std::string getParentDirectory(const std::string &path);
std::string getSharedDirectory(const std::string &dir1, const std::string &dir2);

std::string getEnclosedString(const std::string &s, char a, char b, size_t *pos);

bool cstr_ends_with(const char * str, const char * suffix);

void removeInvalidCharsFromFilename(std::string &filename, bool win32_compatible);

