#include <boost/algorithm/string/trim.hpp>
#include <fstream>
#include <iostream>
#include "curses.h"
#include "bindings.h"
#include "string.h"
#include "wide_string.h"

namespace Generic {

BindingsConfiguration Bindings;

Key Key::noOp = Key(ERR, NCurses);

namespace {//

Key stringToSpecialKey(const std::string &s)
{
	Key result = Key::noOp;
	if (!s.compare("mouse"))
		result = Key(KEY_MOUSE, Key::NCurses);
	else if (!s.compare("up"))
		result = Key(KEY_UP, Key::NCurses);
	else if (!s.compare("down"))
		result = Key(KEY_DOWN, Key::NCurses);
	else if (!s.compare("page_up"))
		result = Key(KEY_PPAGE, Key::NCurses);
	else if (!s.compare("page_down"))
		result = Key(KEY_NPAGE, Key::NCurses);
	else if (!s.compare("home"))
		result = Key(KEY_HOME, Key::NCurses);
	else if (!s.compare("end"))
		result = Key(KEY_END, Key::NCurses);
	else if (!s.compare("space"))
		result = Key(KEY_SPACE, Key::Standard);
	else if (!s.compare("enter"))
		result = Key(KEY_ENTER, Key::Standard);
	else if (!s.compare("insert"))
		result = Key(KEY_IC, Key::NCurses);
	else if (!s.compare("delete"))
		result = Key(KEY_DC, Key::NCurses);
	else if (!s.compare("left"))
		result = Key(KEY_LEFT, Key::NCurses);
	else if (!s.compare("right"))
		result = Key(KEY_RIGHT, Key::NCurses);
	else if (!s.compare("tab"))
		result = Key(KEY_TAB, Key::Standard);
	else if (!s.compare("shift_tab"))
		result = Key(KEY_SHIFT_TAB, Key::NCurses);
	else if (!s.compare(0, 5, "ctrl_") && s.length() > 5 && s[5] >= 'a' && s[5] <= 'z')
		result = Key(KEY_CTRL_A + (s[5] - 'a'), Key::Standard);
	else if (s.length() > 1 && s[0] == 'f')
	{
		int n = atoi(s.c_str() + 1);
		if (n >= 1 && n <= 12)
			result = Key(KEY_F1 + n - 1, Key::NCurses);
	}
	else if (!s.compare("backspace"))
		result = Key(KEY_BACKSPACE, Key::NCurses);
	else if (!s.compare("backspace_2"))
		result = Key(KEY_BACKSPACE_2, Key::Standard);
	return result;
}

Key stringToKey(const std::string &s)
{
	Key result = stringToSpecialKey(s);
	if (result == Key::noOp)
	{
		std::wstring ws = ToWString(s);
		if (ws.length() == 1)
			result = Key(ws[0], Key::Standard);
	}
	return result;
}

template <typename F>
Actions::BaseAction *parseActionLine(Generic::NC::Window* w, const std::string &line, F error)
{
	Actions::BaseAction *result = 0;
	size_t i = 0;
	for (; i < line.size() && !isspace(line[i]); ++i) { }
	if (i == line.size()) // only action name
		result = Actions::BaseAction::get(line);
	else // there is something else
	{
		std::string action_name = line.substr(0, i);
		if (action_name == "push_character")
		{
			// push single character into input queue
			std::string arg = getEnclosedString(line, '"', '"', 0);
			Key k = stringToSpecialKey(arg);
			auto queue = std::vector<int>{ k.getChar() };
			if (k != Key::noOp)
            {
                result = new Actions::PushCharacters(&w, std::move(queue));
            }
			else
				error() << "invalid character passed to push_character: '" << arg << "'\n";
		}
		else if (action_name == "push_characters")
		{
			// push sequence of characters into input queue
			std::string arg = getEnclosedString(line, '"', '"', 0);
			if (!arg.empty())
			{
				std::vector<int> queue(arg.begin(), arg.end());
				// if char is signed, erase 1s from char -> int conversion
				for (auto it = arg.begin(); it != arg.end(); ++it)
					*it &= 0xff;
                result = new Actions::PushCharacters(&w, std::move(queue));
			}
			else
				error() << "empty argument passed to push_characters\n";
		}
	}
	return result;
}

}

Key Key::read(NC::Window &w)
{
	Key result = noOp;
	std::string tmp;
	int input;
	while (true)
	{
		input = w.readKey();
		if (input == ERR)
			break;
		if (input > 255)
		{
			result = Key(input, NCurses);
			break;
		}
		else
		{
			wchar_t wc;
			tmp += input;
			size_t conv_res = mbrtowc(&wc, tmp.c_str(), MB_CUR_MAX, 0);
			if (conv_res == size_t(-1)) // incomplete multibyte character
				continue;
			else if (conv_res == size_t(-2)) // garbage character sequence
				break;
			else // character complete
			{
				result = Key(wc, Standard);
				break;
			}
		}
	}
	return result;
}

bool BindingsConfiguration::read(NC::Window* w, const std::string &file)
{
	enum class InProgress { None, Command, Key };
	
	bool result = true;
	
	std::ifstream f(file);
	if (!f.is_open())
		return result;
	
	// shared variables
	InProgress in_progress = InProgress::None;
	size_t line_no = 0;
	std::string line;
	Binding::ActionChain actions;
	
	// def_key specific variables
	Key key = Key::noOp;
	std::string strkey;
	
	// def_command specific variables
	bool cmd_immediate = false;
	std::string cmd_name;
	
	auto error = [&]() -> std::ostream & {
		std::cerr << file << ":" << line_no << ": error: ";
		in_progress = InProgress::None;
		result = false;
		return std::cerr;
	};
	
	auto bind_in_progress = [&]() -> bool {
		if (in_progress == InProgress::Command)
		{
			if (!actions.empty())
			{
				m_commands.insert(std::make_pair(cmd_name, Command(actions, cmd_immediate)));
				actions.clear();
				return true;
			}
			else
			{
				error() << "definition of command '" << cmd_name << "' cannot be empty\n";
				return false;
			}
		}
		else if (in_progress == InProgress::Key)
		{
			if (!actions.empty())
			{
				bind(key, actions);
				actions.clear();
				return true;
			}
			else
			{
				error() << "definition of key '" << strkey << "' cannot be empty\n";
				return false;
			}
		}
		return true;
	};
	
	const char def_command[] = "def_command";
	const char def_key[] = "def_key";
	
	while (!f.eof() && ++line_no)
	{
		getline(f, line);
		if (line.empty() || line[0] == '#')
			continue;
		
		// beginning of command definition
		if (!line.compare(0, const_strlen(def_command), def_command))
		{
			if (!bind_in_progress())
				break;
			in_progress = InProgress::Command;
			cmd_name = getEnclosedString(line, '"', '"', 0);
			if (cmd_name.empty())
			{
				error() << "command must have non-empty name\n";
				break;
			}
			if (m_commands.find(cmd_name) != m_commands.end())
			{
				error() << "redefinition of command '" << cmd_name << "'\n";
				break;
			}
			std::string cmd_type = getEnclosedString(line, '[', ']', 0);
			if (cmd_type == "immediate")
				cmd_immediate = true;
			else if (cmd_type == "deferred")
				cmd_immediate = false;
			else
			{
				error() << "invalid type of command: '" << cmd_type << "'\n";
				break;
			}
		}
		// beginning of key definition
		else if (!line.compare(0, const_strlen(def_key), def_key))
		{
			if (!bind_in_progress())
				break;
			in_progress = InProgress::Key;
			strkey = getEnclosedString(line, '"', '"', 0);
			key = stringToKey(strkey);
			if (key == Key::noOp)
			{
				error() << "invalid key: '" << strkey << "'\n";
				break;
			}
		}
		else if (isspace(line[0])) // name of action to be bound
		{
			boost::trim(line);
			auto action = parseActionLine(w, line, error);
			if (action)
				actions.push_back(action);
			else
			{
				error() << "unknown action: '" << line << "'\n";
				break;
			}
		}
		else
		{
			error() << "invalid line: '" << line << "'\n";
			break;
		}
	}
	bind_in_progress();
	f.close();
	return result;
}

void BindingsConfiguration::bindKey(std::string name, ActionType action)
{
    Key k = Key::noOp;
    if (notBound(k = stringToKey(name)))
		bind(k, action);
}

}; // end of namespace Generic