#pragma once
#include <string>
#include "gcc.h"

namespace Generic {

void FatalError(const std::string &msg) GNUC_NORETURN;

}; // end of namespace Generic
