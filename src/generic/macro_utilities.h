#pragma once

#include <cassert>
#include <vector>
#include "actions.h"

namespace Generic {

namespace Actions {//
    
struct PushCharacters : public BaseAction
{
	PushCharacters(NC::Window **w, std::vector<int> &&queue)
	: BaseAction(ActionType::MacroUtility, ""), m_window(w), m_queue(queue) { }
	
protected:
	virtual void run();
	
private:
	NC::Window **m_window;
	std::vector<int> m_queue;
};

}; // end of namespace Actions

}; // end of namespace Generic
