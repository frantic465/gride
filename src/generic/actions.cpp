#include "actions.h"

namespace Generic {

namespace Actions {
    
std::map<ActionType, BaseAction *> AvailableActions;

void registerAction(BaseAction* action)
{
    AvailableActions[action->type()] = action;
}

BaseAction* BaseAction::get(ActionType at)
{
	if (AvailableActions.empty())
		return NULL;
	return AvailableActions[at];
}

BaseAction* BaseAction::get(const std::string &name)
{
	BaseAction *result = 0;
	if (AvailableActions.empty())
		return NULL;
	for (auto it = AvailableActions.begin(); it != AvailableActions.end(); ++it)
	{
		if (it->second->name() == name)
		{
			result = it->second;
			break;
		}
	}
	return result;
}

}; // end of namespace Actions

}; // end of namespace Generic