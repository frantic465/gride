#include "window.h"
#include "macro_utilities.h"

namespace Generic {

namespace Actions {//

void PushCharacters::run()
{
	for (auto it = m_queue.begin(); it != m_queue.end(); ++it)
		(*m_window)->pushChar(*it);
}

}; // end of namespace Actions

}; // end of namespace Generic
