#include <ctime>
#include <cstdlib>
#include <iostream>

#include "error.h"

namespace Generic
{
    
const char *Timestamp()
{
    static char result[32];
    time_t raw;
    tm *t;
    time(&raw);
    t = localtime(&raw);
    result[strftime(result, 31, "%Y/%m/%d %X", t)] = 0;
    return result;
}

void FatalError(const std::string &msg)
{
	std::cerr << "[" << Timestamp() << "] " << msg << std::endl;
	abort();
}

}; // end of namespace Generic

