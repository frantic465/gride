#pragma once

#include <string>

std::string ToString(std::wstring ws);
std::wstring ToWString(std::string s);

size_t wideLength(const std::wstring &ws);

void wideCut(std::wstring &ws, size_t max_length);
std::wstring wideShorten(const std::wstring &ws, size_t max_length);

