#pragma once
#include <map>
#include <string>

#include "config.h"

namespace Generic {

namespace Actions {

struct BaseAction
{
    BaseAction(ActionType type_, const char *name_) : m_type(type_), m_name(name_) { }
	
	const char *name() const { return m_name; }
	ActionType type() const { return m_type; }
	
	virtual bool canBeRun() const { return true; }
	
	bool execute()
	{
		if (canBeRun())
		{
			run();
			return true;
		}
		return false;
	}
	
protected:
	virtual void run() = 0;
	
private:
	ActionType m_type;
	const char *m_name;
    
    
public:
    
    static BaseAction *get(ActionType at);
    static BaseAction *get(const std::string &name);
};

void registerAction(BaseAction* action);
    
}; // end of namespace Actions

}; // end of namespace Generic