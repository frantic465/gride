#pragma once

#include <sys/time.h>
#include <stdlib.h>

namespace Generic
{
    class BaseScreen;
    namespace NC
    {
        class Window;
    }
}

namespace Global {//

// header window (above main window)
extern Generic::NC::Window *wHeader;

// footer window (below main window)
extern Generic::NC::Window *wFooter;

// global timer
extern timeval Timer;

}; // end of namespace Global
