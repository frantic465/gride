#include <string>
#include <sstream>
#include <iomanip>

#include "search_dialog_item.h"

SearchDialogItemBase::SearchDialogItemBase(SearchDialogItemType type, const Search::SearchResultItem& searchResultItem)
    : m_type(type)
    , m_searchResultItem(searchResultItem)
{
}

SearchDialogItemBase::~SearchDialogItemBase() 
{
}

//////////////////////////////////////////////////

SearchDialogItemText::SearchDialogItemText(const std::string& text)
    : base(SearchDialogItemType::Text, Search::SearchResultItem())
    , m_text(text)
{
    
}

void SearchDialogItemText::render(Generic::NC::ListBase<SearchDialogItemBaseRef> &list)
{
    list << m_text;
}

//////////////////////////////////////////////////

SearchDialogItemFilename::SearchDialogItemFilename(const Search::SearchResultItem& searchResultItem)
    : base(SearchDialogItemType::Filename, searchResultItem)
{
}

void SearchDialogItemFilename::render(Generic::NC::ListBase<SearchDialogItemBaseRef> &list)
{
    const Search::SearchResultItem& item = getSearchResultItem();
    
    list << Generic::NC::Color::Yellow << Generic::NC::Format::Bold;
    list << item.filename;
    list << Generic::NC::Color::Default << Generic::NC::Format::NoBold;    
}

//////////////////////////////////////////////////

SearchDialogItemContext::SearchDialogItemContext(const Search::SearchResultItem& searchResultItem)
    : base(SearchDialogItemType::Context, searchResultItem)
{
}

void SearchDialogItemContext::render(Generic::NC::ListBase<SearchDialogItemBaseRef> &list)
{
    const Search::SearchResultItem& item = getSearchResultItem();
    
    std::stringstream fline;
    fline << std::setw(6) << item.line << ":" << "  ";
    
	list << Generic::NC::Color::Cyan;
    list << fline.str();        
	
    {
        const int context_start = std::min(item.context.size(), item.start - item.context_offset);
        const int context_end = std::min(item.context.size(), item.end - item.context_offset);
        
        list << Generic::NC::Color::White;
        list << item.context.substr(0, context_start);
        list << Generic::NC::Color::Yellow;
        list << item.context.substr(context_start, context_end-context_start);
        list << Generic::NC::Color::White;
        list << item.context.substr(context_end);
    }
    list << Generic::NC::Color::Default;
}