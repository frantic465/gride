#pragma once

#include <map>
#include <string>
#include "generic/window.h"
#include "generic/actions.h"
#include "generic/macro_utilities.h"

namespace UserActions {//

using Generic::Actions::BaseAction;
using Generic::ActionType;

extern bool ExitMainLoop;

struct Dummy : public BaseAction
{
	Dummy() : BaseAction(ActionType::Dummy, "dummy") { }
	
protected:
	virtual void run() { }
};

struct MouseEvent : public BaseAction
{
	MouseEvent() : BaseAction(ActionType::MouseEvent, "mouse_event") { }
	
protected:
	virtual bool canBeRun() const;
	virtual void run();
	
	private:
		MEVENT m_mouse_event;
		MEVENT m_old_mouse_event;
};

struct ScrollUp : public BaseAction
{
	ScrollUp() : BaseAction(ActionType::ScrollUp, "scroll_up") { }
	
protected:
	virtual void run();
};

struct ScrollDown : public BaseAction
{
	ScrollDown() : BaseAction(ActionType::ScrollDown, "scroll_down") { }
	
protected:
	virtual void run();
};

struct PageUp : public BaseAction
{
	PageUp() : BaseAction(ActionType::PageUp, "page_up") { }
	
protected:
	virtual void run();
};

struct PageDown : public BaseAction
{
	PageDown() : BaseAction(ActionType::PageDown, "page_down") { }
	
protected:
	virtual void run();
};

struct PressEnter : public BaseAction
{
	PressEnter() : BaseAction(ActionType::PressEnter, "press_enter") { }
	
protected:
	virtual void run();
};

struct PressSpace : public BaseAction
{
	PressSpace() : BaseAction(ActionType::PressSpace, "press_space") { }
	
protected:
	virtual void run();
};

struct ExecuteCommand : public BaseAction
{
	ExecuteCommand() : BaseAction(ActionType::ExecuteCommand, "execute_command") { }
	
protected:
	virtual void run();
};

struct FindItemForward : public BaseAction
{
	FindItemForward() : BaseAction(ActionType::FindItemForward, "find_item_forward") { }
	
protected:
	virtual bool canBeRun() const;
	virtual void run();
};

struct FindItemBackward : public BaseAction
{
	FindItemBackward() : BaseAction(ActionType::FindItemBackward, "find_item_backward") { }
	
protected:
	virtual bool canBeRun() const;
	virtual void run();
};

struct NextFoundItem : public BaseAction
{
	NextFoundItem() : BaseAction(ActionType::NextFoundItem, "next_found_item") { }
	
protected:
	virtual bool canBeRun() const;
	virtual void run();
};

struct PreviousFoundItem : public BaseAction
{
	PreviousFoundItem() : BaseAction(ActionType::PreviousFoundItem, "previous_found_item") { }
	
protected:
	virtual bool canBeRun() const;
	virtual void run();
};

struct ToggleFindMode : public BaseAction
{
	ToggleFindMode() : BaseAction(ActionType::ToggleFindMode, "toggle_find_mode") { }
	
protected:
	virtual void run();
};

struct Quit : public BaseAction
{
	Quit() : BaseAction(ActionType::Quit, "quit") { }
	
protected:
	virtual void run();
};

}

