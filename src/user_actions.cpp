#include <cassert>
#include <cerrno>
#include <cstring>
#include <boost/locale/conversion.hpp>
#include <boost/lexical_cast.hpp>
#include <algorithm>
#include <iostream>

#include "generic/config.h"
#include "generic/interfaces.h"
#include "generic/screen.h"

#include "user_actions.h"
#include "global.h"

#include "title.h"

using namespace std::placeholders;
using namespace Generic;

namespace {//

enum class Find { Forward, Backward };

void seek();
void findItem(const Find direction);
void listsChangeFinisher();

}

namespace UserActions {//

bool ExitMainLoop = false;

bool MouseEvent::canBeRun() const
{
	return true;
}

void MouseEvent::run()
{
	
	m_old_mouse_event = m_mouse_event;
	getmouse(&m_mouse_event);
	
	if (m_mouse_event.bstate & (BUTTON1_PRESSED | BUTTON2_PRESSED | BUTTON3_PRESSED | BUTTON4_PRESSED))
		getVisibleScreen()->mouseButtonPressed(m_mouse_event);
}

void ScrollUp::run()
{
	getVisibleScreen()->scroll(NC::Scroll::Up);
}

void ScrollDown::run()
{
	getVisibleScreen()->scroll(NC::Scroll::Down);
}

void PageUp::run()
{
	getVisibleScreen()->scroll(NC::Scroll::PageUp);
}

void PageDown::run()
{
	getVisibleScreen()->scroll(NC::Scroll::PageDown);
}

void PressEnter::run()
{
	getVisibleScreen()->enterPressed();
}

void PressSpace::run()
{
	getVisibleScreen()->spacePressed();
}

void ExecuteCommand::run()
{
	using Global::wFooter;
	
}

bool FindItemBackward::canBeRun() const
{
	auto w = dynamic_cast<Searchable *>(getVisibleScreen());
	return w && w->allowsSearching();
}

void FindItemForward::run()
{
	findItem(::Find::Forward);
}

bool FindItemForward::canBeRun() const
{
	auto w = dynamic_cast<Searchable *>(getVisibleScreen());
	return w && w->allowsSearching();
}

void FindItemBackward::run()
{
	findItem(::Find::Backward);
}

bool NextFoundItem::canBeRun() const
{
	return dynamic_cast<Searchable *>(getVisibleScreen());
}

void NextFoundItem::run()
{
	Searchable *w = dynamic_cast<Searchable *>(getVisibleScreen());
	w->nextFound(true);
}

bool PreviousFoundItem::canBeRun() const
{
	return dynamic_cast<Searchable *>(getVisibleScreen());
}

void PreviousFoundItem::run()
{
	Searchable *w = dynamic_cast<Searchable *>(getVisibleScreen());
	w->prevFound(true);
}

void ToggleFindMode::run()
{
	
}

void Quit::run()
{
	ExitMainLoop = true;
}

}

namespace {//



void findItem(const Find direction)
{
	using Global::wFooter;
	
	Searchable *w = dynamic_cast<Searchable *>(getVisibleScreen());
	assert(w);
	assert(w->allowsSearching());
	
}

}

