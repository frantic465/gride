#pragma once

#include <boost/shared_ptr.hpp>
#include "generic/list_base.h"
#include "search/grep.h"

class SearchDialogItemBase;
class SearchDialogItemContext;
typedef boost::shared_ptr<SearchDialogItemBase> SearchDialogItemBaseRef;
typedef boost::shared_ptr<SearchDialogItemContext> SearchDialogItemRef;

enum class SearchDialogItemType
{
    Invalid,
    Text,
    Filename,
    Context,
};

class SearchDialogItemBase {
public:
    SearchDialogItemBase(SearchDialogItemType type, const Search::SearchResultItem& searchResultItem);
    virtual ~SearchDialogItemBase();
    
    const Search::SearchResultItem& getSearchResultItem() const { return m_searchResultItem; }
    
    SearchDialogItemType getType() const { return m_type; }
    virtual void render(Generic::NC::ListBase<SearchDialogItemBaseRef> &list) = 0;
private:
    const SearchDialogItemType m_type;
    const Search::SearchResultItem m_searchResultItem;
};


class SearchDialogItemText
    : public SearchDialogItemBase 
{
    typedef SearchDialogItemBase base;
public:
    SearchDialogItemText(const std::string& text = std::string());
    virtual void render(Generic::NC::ListBase<SearchDialogItemBaseRef> &list);
    
private:
    const std::string m_text;
};

class SearchDialogItemFilename
    : public SearchDialogItemBase 
{
    typedef SearchDialogItemBase base;
public:
    SearchDialogItemFilename(const Search::SearchResultItem& searchResultItem);
    virtual void render(Generic::NC::ListBase<SearchDialogItemBaseRef> &list);
};

class SearchDialogItemContext
    : public SearchDialogItemBase 
{
    typedef SearchDialogItemBase base;
public:
    SearchDialogItemContext(const Search::SearchResultItem& searchResultItem);
    virtual void render(Generic::NC::ListBase<SearchDialogItemBaseRef> &list);
};

