#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <pcrecpp.h>
#include <dirent.h>
#include <list>
#include <algorithm>
#include <unistd.h>

#include "grep.h"
#include "ctags.h"
#include "src/generic/string.h"

namespace Search
{
GrepBuilder::GrepBuilder(const std::string& pattern)
    : m_pattern(pattern)
    , m_icase(false)
    , m_searchSymbols(false)
    , m_re("")
    , m_buffer(NULL)
    , m_buffersize(getpagesize())
{
    {
        m_excludedDirs.push_back(".git");
        m_excludedDirs.push_back("CVS");
        m_excludedDirs.push_back(".dot");
        m_excludedDirs.push_back(".hg");
        m_excludedDirs.push_back(".pc");
        m_excludedDirs.push_back(".svn");
    }
}

GrepBuilder& GrepBuilder::ignoreCase()
{
    m_icase = true;
    return *this;
}

GrepBuilder& GrepBuilder::languageOnly(const std::string& language)
{
    m_language = language;
    return *this;
}

GrepBuilder& GrepBuilder::searchSymbols()
{
    m_searchSymbols = true;
    return *this;
}

bool GrepBuilder::skipDirectory(const char* dir) const
{
    for (auto it = m_excludedDirs.begin(); it != m_excludedDirs.end(); ++it)
    {
        if (it->compare(dir) == 0)
        {
            return true;
        }
    }
    return false;
}

bool GrepBuilder::skipFile(const char* file) const
{
    if (m_allowedExtensions.empty())
    {
        return false;
    }
    for (auto it = m_allowedExtensions.begin(); it != m_allowedExtensions.end(); ++it)
    {
        if ( cstr_ends_with(file, it->c_str()) )
        {
            return false;
        }
    }
    return true;
}

void GrepBuilder::processDirRecursively(const char* dir)
{
    DIR* d = opendir (dir);

    if (! d) {
        std::cerr << "Cannot open directory " << dir << std::endl;
        exit (EXIT_FAILURE);
    }
    
    struct dirent * entry;
        
    while ( (entry = readdir(d)) ) {
        const char * d_name;
        d_name = entry->d_name;
        
        int path_length;
        char path[PATH_MAX];

        path_length = snprintf (path, PATH_MAX, "%s/%s", dir, d_name);
        if (path_length >= PATH_MAX) {
            std::cerr << "Path length has got too long." << std::endl;
            exit (EXIT_FAILURE);
        }
        
        if (entry->d_type & DT_DIR) 
        {
            if (strcmp (d_name, "..") == 0 || strcmp (d_name, ".") == 0 || skipDirectory(d_name)) 
            {
                continue;
            }
            processDirRecursively(path);
        }
        else
        {
            if (skipFile(path))
            {
                continue;
            }
            processFile(path);
        }
    }
    
    if (closedir (d)) {
        std::cerr << "Could not close " << dir << std::endl;
        exit (EXIT_FAILURE);
    }
}

bool GrepBuilder::breakProcessing(const char* buffer, int len) const
{
    return memchr(buffer, 0, len) != 0;
}

void GrepBuilder::processBuffer(const char* buffer, int len)
{
    pcrecpp::StringPiece matched;
    pcrecpp::StringPiece input(buffer, len);
    while (m_re.FindAndConsume(&input, &matched))
    {
        const int contextsize = std::min(100, len);
        const int start = matched.data() - buffer;
        const int end = start + matched.size();
        const int context_offset = end > contextsize 
            ? std::max(0, start - contextsize/5) 
            : 0;

        const std::string context(buffer + context_offset, buffer + context_offset + contextsize);

        m_result->push_back(
            SearchResultItem(
                m_currentFile, 
                m_linenum, 
                start, 
                end,
                context_offset,
                context
            ));
    }
}

void GrepBuilder::processStream(std::istream& stream)
{
    const int maxLineSize = 128;
    int limit = 0;
    char* beg = 0;
    char* endl = 0;
    int remainder = 0;
    int chunkIndex = 0;
            
    m_linenum = 1;

    while(stream.good())
    {
        stream.seekg(-remainder, std::ios_base::cur);
        stream.read(m_buffer, m_buffersize);
        beg = m_buffer;
        limit = stream.gcount();
        
        if (chunkIndex == 0 && limit > 0 && breakProcessing(beg, limit))
        {
            break;
        }
        while ( limit > 0 )
        {
            endl = (char*)memchr(beg, '\n', limit);
            if ( endl == NULL )
            {
                break;
            }
            int linesize = endl - beg;
            processBuffer(beg, linesize);
            limit -= (linesize + 1);
            beg = endl + 1;
            ++m_linenum;
        }
        
        remainder = std::min(maxLineSize, std::max(0, limit));
        ++chunkIndex;
    }
}    

void GrepBuilder::processFile(const char* name)
{
    std::ifstream is(name);
    if(is.bad())
    {
        std::cerr << "Unable to open file " << name << std::endl;
    }

    m_currentFile = name;
    {
        processStream(is);
    }
    m_currentFile = NULL;
}

void GrepBuilder::processTags()
{
    CTagsGenerator tags;
    tags.generate(".");
    
    std::ifstream stream(".gridectags");
    std::string regex_word = "(\\w+)";
    std::string regex_filename = "(\.+)";
    std::string regex_line = "(\\d+)";
    std::string regex_end = "\\;\"";
    std::string regex_type = "(\\w)";
    std::string regex_ctx = "(\.+)?";
    std::stringstream pattern;
    pattern << regex_word << "\\t"
            << regex_filename << "\\t"
            << regex_line << regex_end << "\\t"
            << regex_type << "\\t?"
            << regex_ctx;
            
    pcrecpp::RE re(pattern.str().c_str());
    std::string buffer;
    std::string symbol, filename, line, type, ctx;
    pcrecpp::StringPiece matched;
    
    while (std::getline(stream, buffer))
    {
        if (!re.FullMatch(buffer, &symbol, &filename, &line, &type, &ctx))
        {
            continue;
        }
        
        pcrecpp::StringPiece symbolBuffer(symbol.c_str(), symbol.size());
        if (!m_re.PartialMatch(symbolBuffer, &matched))
        {
            continue;
        }
        
        const int linenum = atoi(line.c_str());
        const int start = matched.data() - symbolBuffer.data();
        const int end = start + matched.size();;
        const int context_offset = 0;
        const std::string context = symbol + "      " + ctx;
        
        m_result->push_back(
            SearchResultItem(
                filename,
                linenum, 
                start, 
                end,
                context_offset,
                context
            ));
    }
}

std::vector<std::string> GrepBuilder::getAllowedExtensionsList(const std::string& language) const
{
    std::vector<std::string> result;
    if (language == "java" || language == "j")
    {
        result.push_back(".java");
    }
    else if (language == "c")
    {
        result.push_back(".c");
        result.push_back(".cpp");
        result.push_back(".cxx");
        result.push_back(".h");
    }
    return result;
}

void GrepBuilder::grep(SearchResult& result)
{
    m_result = &result;
    std::stringstream formattedPattern;
    formattedPattern << "(" << m_pattern << ")";
    
    if (!m_language.empty())
    {
        m_allowedExtensions = getAllowedExtensionsList(m_language);
        if (m_allowedExtensions.empty())
        {
            std::cerr << "language " << m_language << " is not supported" << std::endl;
            exit(1);
        }
    }
    
    pcrecpp::RE_Options options;
    options.set_caseless(m_icase);
    m_re = pcrecpp::RE(formattedPattern.str().c_str(), options);
    
    m_buffer = new char[m_buffersize];
    
    if (m_searchSymbols)
    {
        processTags();
    }
    else
    {
        processDirRecursively(".");
    }
    
    delete [] m_buffer;
    m_buffer = 0;
}
    
}; // end of namespace Grep
