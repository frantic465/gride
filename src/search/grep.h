#pragma once

#include <stdlib.h>
#include <limits.h>
#include <list>
#include <pcrecpp.h>

namespace Search
{
    
    struct SearchResultItem
    {
        SearchResultItem()
          : line(UINT_MAX), start(UINT_MAX), end(UINT_MAX), context_offset(UINT_MAX)
        {}
        
        SearchResultItem(const std::string& filename, size_t line, size_t start, size_t end, size_t context_offset, const std::string& context) 
          : filename(filename), line(line), start(start), end(end), context_offset(context_offset), context(context) 
        {}

        bool isEmpty() const { return line == UINT_MAX; }

        const std::string filename;
        const size_t line;
        const size_t start;
        const size_t end;
        const size_t context_offset;
        const std::string context;
    };
    
    typedef std::list<SearchResultItem> SearchResult;
    
    class GrepBuilder
    {
    public:
        GrepBuilder(const std::string& pattern);
        
        GrepBuilder& ignoreCase();
        GrepBuilder& languageOnly(const std::string& language);
        GrepBuilder& searchSymbols();
                
        void grep(SearchResult& result);         
        
    private:
        std::vector<std::string> getAllowedExtensionsList(const std::string& language) const;
        bool breakProcessing(const char* buffer, int len) const;
        bool skipDirectory(const char* dir) const;
        bool skipFile(const char* dir) const;
        void processDirRecursively(const char* dir);
        void processFile(const char* name);
        void processStream(std::istream& stream);
        void processBuffer(const char* buffer, int len);        
        
        void processTags();
        
    private:
        SearchResult* m_result;
        const char* m_currentFile;
        size_t m_linenum;
        std::string m_pattern;
        std::string m_language;
        bool m_icase;        
        bool m_searchSymbols;
        pcrecpp::RE m_re;
        std::vector<std::string> m_excludedDirs;
        std::vector<std::string> m_allowedExtensions;
        char* m_buffer;
        const int m_buffersize;
    };
    
}; // end of namespace Grep
