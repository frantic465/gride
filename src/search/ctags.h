#pragma once

namespace Search
{

class CTagsGenerator {
public:
    CTagsGenerator();
    void generate(const std::string& dir);
    virtual ~CTagsGenerator();
private:

};

}; // end of namespace Search


