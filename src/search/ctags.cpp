#include <fstream>
#include <iostream>
#include <sstream>
#include <pcrecpp.h>
#include <list>
#include "ctags.h"

namespace Search 
{

CTagsGenerator::CTagsGenerator() {
}

CTagsGenerator::~CTagsGenerator() {
}

void CTagsGenerator::generate(const std::string& dir)
{
    std::stringstream cmd;
    cmd     << "ctags "
            << "-Rn "
            << "-f " << ".gridectags "
            << dir;
            
    system(cmd.str().c_str());
}

}; // end of namespace Search