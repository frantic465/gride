#include <cstring>
#include <iostream>

#include "generic/window.h"
#include "generic/screen.h"

#include "global.h"
#include "title.h"

#ifdef USE_PDCURSES
void windowTitle(const std::string &) { }
#else
void windowTitle(const std::string &status)
{
	if (strcmp(getenv("TERM"), "linux"))
		std::cout << "\033]0;" << status << "\7";
}
#endif // USE_PDCURSES

void drawHeader()
{
	using Global::wHeader;
    using namespace Generic;
	
    *wHeader << NC::XY(0, 0) << wclrtoeol << NC::Format::Bold << getVisibleScreen()->title() << NC::Format::NoBold;
	wHeader->refresh();
}
