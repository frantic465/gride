#include <cerrno>
#include <clocale>
#include <csignal>
#include <cstring>
#include <sys/time.h>

#include <boost/locale.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>
#include <stdexcept>

#include "generic/window.h"
#include "generic/bindings.h"
#include "generic/config.h"

#include "search/grep.h"
#include "search/ctags.h"

#include "global.h"
#include "title.h"
#include "user_actions.h"
#include "search_dialog.h"

#define VERSION "0.1"

namespace po = boost::program_options;

void setResizeFlags()
{
    searchDialog->hasToBeResized = true;
}

void resizeScreen(bool reload_main_window)
{
    using Generic::MainHeight;
    using Global::wHeader;
    using Global::wFooter;

#	if defined(USE_PDCURSES)
    resize_term(0, 0);
#	else
    // update internal screen dimensions
    if (reload_main_window)
    {
        endwin();
        refresh();
        // get rid of KEY_RESIZE as it sometimes
        // corrupts our new cool ReadKey() function
        // because KEY_RESIZE doesn't come from stdin
        // and thus select cannot detect it
        timeout(10);
        getch();
        timeout(-1);
    }
#	endif

    MainHeight = LINES-(4);

    Generic::validateScreenSize();

    setResizeFlags();

    Generic::applyToVisibleWindows(&Generic::BaseScreen::resize);

    wHeader->resize(COLS, 1);

    wFooter->moveTo(0, LINES-(2));
    wFooter->resize(COLS, 2);

    Generic::applyToVisibleWindows(&Generic::BaseScreen::refresh);

    drawHeader();
}

void initializeScreens()
{
    using Global::wFooter;
    using Global::wHeader;
    
    {
        wHeader = new Generic::NC::Window(0, 0, COLS, 1, "", Generic::NC::Color::Default, Generic::NC::Border::None);
        wHeader->display();
    }
    
    {
        wFooter = new Generic::NC::Window(0, LINES - 1, COLS, 1, "", Generic::NC::Color::Default, Generic::NC::Border::None);
        wFooter->setTimeout(500);
        wFooter->createHistory();
    }
    
    {
        searchDialog = new SearchDialog();
        searchDialog->switchTo();
    }
}

void registerActions()
{
    auto insert_action = [](Generic::Actions::BaseAction *a) {
        Generic::Actions::registerAction(a);
	};
	insert_action(new UserActions::Dummy());
	insert_action(new UserActions::MouseEvent());
	insert_action(new UserActions::ScrollUp());
	insert_action(new UserActions::ScrollDown());
	insert_action(new UserActions::PageUp());
	insert_action(new UserActions::PageDown());
	insert_action(new UserActions::PressEnter());
	insert_action(new UserActions::PressSpace());
	insert_action(new UserActions::ExecuteCommand());
	insert_action(new UserActions::FindItemForward());
	insert_action(new UserActions::FindItemBackward());
	insert_action(new UserActions::NextFoundItem());
	insert_action(new UserActions::PreviousFoundItem());
	insert_action(new UserActions::ToggleFindMode());
	insert_action(new UserActions::Quit());
}

void bindKeysToActions()
{
    using Generic::ActionType;
    Generic::Bindings.bindKey("mouse", ActionType::MouseEvent);
	Generic::Bindings.bindKey("up", ActionType::ScrollUp);
		
	Generic::Bindings.bindKey("down", ActionType::ScrollDown);
	Generic::Bindings.bindKey("page_up", ActionType::PageUp);
	Generic::Bindings.bindKey("page_down", ActionType::PageDown);
	Generic::Bindings.bindKey("space", ActionType::PressSpace);
	Generic::Bindings.bindKey("enter", ActionType::PressEnter);
	Generic::Bindings.bindKey(":", ActionType::ExecuteCommand);
	Generic::Bindings.bindKey("/", ActionType::FindItemForward);
	Generic::Bindings.bindKey("?", ActionType::FindItemBackward);
	Generic::Bindings.bindKey(".", ActionType::NextFoundItem);
	Generic::Bindings.bindKey(",", ActionType::PreviousFoundItem);
	Generic::Bindings.bindKey("w", ActionType::ToggleFindMode);
	Generic::Bindings.bindKey("q", ActionType::Quit);
}

void doGrep(int argc, char *argv[], Search::SearchResult& result)
{
    po::options_description opts("Options");
    opts.add_options()
            ("help,h",                                          "display this help and exit")
            ("ignore-case,i",                                   "ignore case distinctions")
            ("symbols,s",                                       "find symbol")
            ("language,l",      po::value<std::string>(),       "language")
            ;
    
    po::options_description hidden("Hidden options");
    hidden.add_options()
            ("regexp",            "use PATTERN for matching")
            ;
    
    po::options_description cmdline_options;
    cmdline_options.add(opts).add(hidden);
    
    po::positional_options_description positional_opts;
    positional_opts.add("regexp", -1);
    
    po::variables_map vm;
    try
    {
        po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(positional_opts).run(), vm);
        po::notify(vm);
    }
    catch (const boost::program_options::error& e)
    {
        std::cerr << "Exception while argument parsing '" << e.what() << "'" << std::endl;
        std::cerr << "Usage: " APPNAME " [OPTION]... PATTERN" << std::endl <<
                     "Try '" APPNAME " --help' for more information." << std::endl;
        exit(2);
    }
    
    if (vm.count("help"))
    {
        std::cout << "Usage: " APPNAME " [OPTION]... PATTERN" << std::endl <<
                     opts << std::endl;
        exit(0);
    }
    
    std::string pattern;
    if (vm.count("regexp"))
    {
        pattern = vm["regexp"].as< std::string >();
    }
    else
    {
        std::cout << "Usage: " APPNAME " [OPTION]... PATTERN" << std::endl <<
                     "Try '" APPNAME " --help' for more information." << std::endl;
        exit(1);
    }
    
    Search::GrepBuilder grepBuilder = Search::GrepBuilder(pattern);
    
    if (vm.count("ignore-case"))
    {
        grepBuilder.ignoreCase();
    }
    
    if (vm.count("language"))
    {
        grepBuilder.languageOnly(vm["language"].as<std::string>());
    }
    
    if (vm.count("symbols"))
    {
        grepBuilder.searchSymbols();
    }
    
    grepBuilder.grep(result);
}

# if !defined(WIN32)
void sighandler(int signal)
{
    if (signal == SIGPIPE)
    {
        //Statusbar::msg("SIGPIPE (broken pipe signal) received");
    }
    else if (signal == SIGWINCH)
    {
        resizeScreen(true);
    }
    else if (signal == SIGUSR1)
    {
        resizeScreen(true);
    }
}
# endif // !WIN32

void do_at_exit()
{
    Generic::NC::destroyScreen();
    windowTitle("");
}

int main(int argc, char *argv[])
{
    using Global::wHeader;
    using Global::wFooter;
    using Global::Timer;

    std::srand(std::time(0));
    std::setlocale(LC_ALL, "");
    
    Search::SearchResult searchResult;
    
    doGrep(argc, argv, searchResult);
    
#if _PROFILE
    std::cout << APPNAME " was built in profile mode" << std::endl;
    return 0;
#endif
    
    atexit(do_at_exit);
    
    Generic::NC::initScreen(APPNAME "ver. " VERSION, true);    
    Generic::setWindowsDimensions(0, 1, 0, LINES - 2);
    Generic::validateScreenSize();
    
    registerActions();
    bindKeysToActions();    
    initializeScreens();    
    
    {
        searchDialog->main().reserve(searchResult.size());
        std::string currentFile;
        for (auto it = searchResult.begin(); it != searchResult.end(); ++it)
        {
            if (it->filename != currentFile)
            {
                searchDialog->main().addItem(SearchDialogItemBaseRef(new SearchDialogItemText()));
                searchDialog->main().addItem(SearchDialogItemBaseRef(new SearchDialogItemFilename(*it)));
                currentFile = it->filename;
            }
            searchDialog->main().addItem(SearchDialogItemBaseRef(new SearchDialogItemContext(*it)));
        }
    }
    
    // initialize global timer
	gettimeofday(&Timer, 0);
    
    Generic::Key input(0, Generic::Key::Standard);
    
# ifndef WIN32
    signal(SIGPIPE, sighandler);
    signal(SIGWINCH, sighandler);
    signal(SIGUSR1, sighandler);
# endif // !WIN32


    mouseinterval(0);
    mousemask(ALL_MOUSE_EVENTS, 0);

    while (!UserActions::ExitMainLoop)
    {
        drawHeader();
        
        if (input != Generic::Key::noOp)
        {
			Generic::getVisibleScreen()->refreshWindow();
        }
        
        input = Generic::Key::read(*wFooter);

        if (input == Generic::Key::noOp)
            continue;

        auto k = Generic::Bindings.get(input);
        for (; k.first != k.second; ++k.first)
            if (k.first->execute())
                break;        
    }
    
    return 0;
}

