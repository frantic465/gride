# -*- coding: utf-8 -*-

import os
import commands
import string

EnsureSConsVersion(0, 96)


# ----------------------------------------------------------------------
# Function definitions
# ----------------------------------------------------------------------

def CheckPKGConfig(context):
	context.Message('Checking for pkg-config... ')
	ret = context.TryAction('pkg-config --version')[0]
	context.Result(ret)
	return ret

def CheckPKG(context, name):
	context.Message('Checking for %s... ' % name)
	ret = context.TryAction('pkg-config --exists \'%s\'' % name)[0]
	context.Result(ret)
	return ret

def CheckCXXVersion(context, name, major, minor):
	context.Message('Checking for %s >= %d.%d...' % (name, major, minor))
	ret = commands.getoutput('%s -dumpversion' % name)

	retval = 0
	try:
		if ((string.atoi(ret[0]) == major and string.atoi(ret[2]) >= minor)
		or (string.atoi(ret[0]) > major)):
			retval = 1
	except ValueError:
		print "No C++ compiler found!"

	context.Result(retval)
	return retval


# ----------------------------------------------------------------------
# Command-line options
# ----------------------------------------------------------------------

# Parameters are only sticky from scons -> scons install, otherwise they're cleared.
if 'install' in COMMAND_LINE_TARGETS:
	opts = Options('build/sconf/scache.conf')
else:
	opts = Options()

opts.AddOptions(
  BoolOption('debug', 'Compile the program with debug information', 0),
  BoolOption('release', 'Compile the program with optimizations', 0),
  BoolOption('profile', 'Compile the program with profiling information', 0),
  PathOption('PREFIX', 'Compile the program with PREFIX as the root for installation', '/usr/local'),
)


# ----------------------------------------------------------------------
# Initialization
# ----------------------------------------------------------------------

env = Environment(ENV = os.environ, options = opts)

conf = Configure(env,
	custom_tests =
	{
		'CheckPKGConfig' : CheckPKGConfig,
		'CheckPKG' : CheckPKG,
		'CheckCXXVersion' : CheckCXXVersion
	},
	conf_dir = 'build/sconf',
	log_file = 'build/sconf/config.log')

if os.environ.has_key('CXX'):
	conf.env['CXX'] = os.environ['CXX']

if os.environ.has_key('CC'):
	conf.env['CC'] = os.environ['CC']

if os.environ.has_key('CXXFLAGS'):
	conf.env['CPPFLAGS'] = conf.env['CXXFLAGS'] = os.environ['CXXFLAGS'].split()

if os.environ.has_key('LDFLAGS'):
	conf.env['LINKFLAGS'] = os.environ['LDFLAGS'].split()

if os.environ.has_key('CFLAGS'):
	conf.env['CFLAGS'] = os.environ['CFLAGS'].split()

env.SConsignFile('build/sconf/.sconsign')
opts.Save('build/sconf/scache.conf', env)
Help(opts.GenerateHelpText(env))

# ----------------------------------------------------------------------
# Dependencies
# ----------------------------------------------------------------------

if not 'install' in COMMAND_LINE_TARGETS:

	if not (conf.env.has_key('CXX') and conf.env['CXX']):
		print 'CXX env variable is not set, attempting to use g++'
		conf.env['CXX'] = 'g++'

	if not conf.CheckCXXVersion(env['CXX'], 4, 6):
		print 'Compiler version check failed. g++ 4.6 or later is needed'
		Exit(1)

	if not conf.CheckPKGConfig():
		print '\tpkg-config not found.'
		Exit(1)

	if not conf.CheckPKG('ncursesw >= 5.9'):
		print '\tncursesw >= 5.9 not found.'
		print '\tNote: You might have the lib but not the headers'
		Exit(1)

	if not conf.CheckPKG('libpcrecpp'):
		print '\tlibpcrecpp not found.'
		print '\tNote: You might have the lib but not the headers'
		Exit(1)

	if not conf.CheckHeader('time.h'):
		Exit(1)

	if not conf.CheckHeader('signal.h'):
		Exit(1)

	if not conf.CheckHeader('unistd.h'):
		Exit(1)

	libraries = [ 
		'boost_locale-mt',
		'boost_regex-mt',
		'boost_program_options-mt',
		'boost_system-mt',
		'magic',
	]
	
	for lib in libraries:
		if not conf.CheckLib(lib):
			print '\t%s library not found' % (lib)
			print '\tNote: You might have the lib but not the headers'
			Exit(1)
	
	env = conf.Finish()


# ----------------------------------------------------------------------
# Compile and link flags
# ----------------------------------------------------------------------

	env.Append(CXXFLAGS = [
		'-I.', 
		'-std=c++0x', 
		'-fexceptions', 
		'-frtti', 
		'-Wno-deprecated-declarations', 
		'-Werror=return-type', 
		'-Wno-unused-value',
		'-Wno-switch',
        ])

	env.Append(LINKFLAGS = [
		'-lboost_locale-mt',
		'-lboost_regex-mt',
		'-lboost_program_options-mt',
		'-lboost_system-mt',
		'-lmagic',
		])
	
	env.ParseConfig('pkg-config --libs ncursesw')
	env.ParseConfig('pkg-config --cflags ncursesw')
	env.ParseConfig('pkg-config --libs libpcrecpp')
	env.ParseConfig('pkg-config --cflags libpcrecpp')
	
	if env.has_key('debug') and env['debug']:
		env.Append(CXXFLAGS = ['-g', '-ggdb', '-D_DEBUG', '-Wall'])
		env.Append(LINKFLAGS = ['-g', '-ggdb' '-Wall'])

	if env.has_key('release') and env['release']:
		env.Append(CXXFLAGS = ['-O2', '-fomit-frame-pointer']) #, '-DNDEBUG'])

	if env.has_key('profile') and env['profile']:
		env.Append(CXXFLAGS = ['-pg', '-D_PROFILE'])
		env.Append(LINKFLAGS= '-pg')

	if env.has_key('PREFIX') and env['PREFIX']:
		env.Append(CXXFLAGS = '-D_DATADIR=\'\"' + env['PREFIX'] + '/share' + '\"\'')

	
# ----------------------------------------------------------------------
# Build
# ----------------------------------------------------------------------

	build = env.Program(
		target = 'gride', 
		source = [SConscript('src/SConstruct', exports='env', build_dir='build', duplicate=0)]
	)
	Default(build)

# ----------------------------------------------------------------------
# Install
# ----------------------------------------------------------------------

else:

	text_files = [
		'Changelog.txt',
		'Credits.txt',
		'License.txt',
		'Readme.txt']

	env.Alias('install', env.Install(dir = env['PREFIX'] + '/share/doc/gride', source = text_files))
	env.Alias('install', env.Install(dir = env['PREFIX'] + '/bin', source = 'gride'))
